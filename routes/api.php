<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api'], function () {
    Route::get('catalog', '\App\Http\Controllers\Api\CatalogController@index');
});

Route::group(['namespace' => 'Api'], function () {
    Route::middleware('auth:api')->post('basket/add/{catalog}',  '\App\Http\Controllers\Api\BasketController@add');
    Route::middleware('auth:api')->post('basket/delete/{basket}',  '\App\Http\Controllers\Api\BasketController@delete');
    Route::middleware('auth:api')->get('basket/list',  '\App\Http\Controllers\Api\BasketController@list');
});
