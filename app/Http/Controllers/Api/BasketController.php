<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Basket;
use App\Models\Catalog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BasketController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @param Catalog $catalog_id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function add(Request $request, Catalog $catalog)
    {
        Basket::create([
            'catalog_id' => $catalog->id,
            'user_id' => auth()->id()
        ]);
        return Basket::where(['user_id' => auth()->id()])->count();
    }


    public function delete(Request $request, Basket $basket)
    {
        $basket->delete();

        return Basket::where(['user_id' => auth()->id()])->with('catalog')->get();
    }


    public function list(Request $request, Catalog $catalog)
    {
        return Basket::where(['user_id' => auth()->id()])->with('catalog')->get();
    }
}
